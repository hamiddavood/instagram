from django.urls import path
from .views import TagListAPI, TagDetailAPI, PostDetailAPI
from apps import content

urlpatterns = [
    path('tags/', TagListAPI.as_view(), name='tags'),
    path('tag/<int:pk>/', TagDetailAPI.as_view(), name='tag'),
    path('post/<int:pk>/', PostDetailAPI.as_view(), name='post'),
]
