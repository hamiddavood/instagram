from rest_framework import serializers

from apps.activity.serializers import CommentListSerializer
from apps.location.serializers import LocationSerializer
from apps.content.models import Tag, Post, PostMedia


class TagListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'title',)

    def create(self, validated_data):
        instance = super().create(validated_data)
        return instance


class PostMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostMedia
        fields = ('id', 'media_type', 'media_file',)


class TagDetailSerializer(TagListSerializer):
    posts = serializers.SerializerMethodField()

    class Meta:
        model = Tag
        fields = ('id', 'title',)

    def get_posts(self, obj):
        return obj.posts.counts()


class PostDetailSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="user.username")
    location = LocationSerializer()
    media = PostMediaSerializer(many=True)
    comments = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ('caption', 'user', 'location', 'media', 'comments')

    def get_comments(self, obj):
        serializer = CommentListSerializer(obj.comments.filter(reply_to__isnull=True),many=True)
        return serializer.data


