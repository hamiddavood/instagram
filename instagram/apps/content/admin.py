from django.contrib import admin
from django.contrib.admin import register

from .models import Post, PostMedia, PostTag, Tag, TaggedUser


@register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('user',)


@register(PostMedia)
class PostMediaAdmin(admin.ModelAdmin):
    list_display = ('media_type',)


@register(PostTag)
class PotTagAdmin(admin.ModelAdmin):
    list_display = ('tag',)

@register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('title',)

@register(TaggedUser)
class TaggedUserAdmin(admin.ModelAdmin):
    list_display = ('user',)