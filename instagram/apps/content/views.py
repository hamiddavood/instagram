from django.shortcuts import render

# Create your views here.
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Tag, Post
from .serializers import TagListSerializer, PostDetailSerializer
from rest_framework import status
from rest_framework.generics import get_object_or_404, ListAPIView


class TagListAPI(ListAPIView):
    queryset = Tag.objects.all()
    # permission_classes = (IsAuthenticated,)
    serializer_class = TagListSerializer




class TagDetailAPI(APIView):
    def get(self, request, pk, *args, **kwargs):
        tag = get_object_or_404(Tag, **{'pk': pk})
        serialized_data = TagListSerializer(tag)
        return Response(serialized_data.data, status=status.HTTP_200_OK)


class PostDetailAPI(APIView):
    def get(self, request, pk, *args, **kwargs):
        instance = get_object_or_404(Post, **{'pk': pk})
        serialized_data = PostDetailSerializer(instance)
        return Response(serialized_data.data, status=status.HTTP_200_OK)
