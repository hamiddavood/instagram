from django.contrib import admin
from django.contrib.admin import register

from .models import Relation


@register(Relation)
class RelationAdmin(admin.ModelAdmin):
    list_display = ('from_user',)


