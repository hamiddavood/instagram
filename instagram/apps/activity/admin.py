from django.contrib import admin
from django.contrib.admin import register

from .models import Comment, Like


@register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('user',)


@register(Like)
class LikeAdmin(admin.ModelAdmin):
    list_display = ('user',)
