from django.urls import path
from .views import CommentListCreateAPI, CommentRetrieveUpdateDestroyAPIView
from apps import content

urlpatterns = [
    path('comment/create/', CommentListCreateAPI.as_view(), name='comment-create'),
    path('comment/retrieve/<int:pk>/', CommentRetrieveUpdateDestroyAPIView.as_view(), name='comment-retrieve'),
]
